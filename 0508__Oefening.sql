use Modernways;
DROP TABLE IF EXISTS Liedjes;
CREATE TABLE Liedjes (
  Titel varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  Artiest varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  Jaar char(4) DEFAULT NULL,
  Album varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
);
LOCK TABLES Liedjes WRITE;
INSERT INTO Liedjes VALUES ('John the Revelator','Larkin Poe','2017','Peach'),('Missionary Man','Ghost','2016','Popestar');
UNLOCK TABLES;