use modernways;
drop table if exists Studenten;
create table Studenten(Voornaam VARCHAR(80) CHAR SET utf8mb4,
    Familienaam VARCHAR(80) CHAR SET utf8mb4,
    Studentennummer INT auto_increment primary key
    );

drop table if exists Opleidingen;
create table Opleidingen(
	Naam VARCHAR(80) CHAR SET utf8mb4 auto_increment primary key
);

drop table if exists Vak;
create table Vak(
	Naam VARCHAR(80) CHAR SET utf8mb4 auto_increment primary key
);


drop table if exists Lector;
create table Lector(Voornaam VARCHAR(80) CHAR SET utf8mb4,
    Familienaam VARCHAR(80) CHAR SET utf8mb4,
    Personeelnummer INT auto_increment primary key
);

