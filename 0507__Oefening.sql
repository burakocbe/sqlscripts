use Modernways;
drop table if exists Metingen;
create table Metingen(
Tijdstip datetime NOT NULL,
Grootte smallint(5) unsigned NOT NULL,
Marge float(3,2) NOT NULL
);