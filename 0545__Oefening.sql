USE ModernWays;
SELECT Artiest, SUM(Aantalbeluisteringen) as 'Totaal aantal'
FROM Liedjes
WHERE LENGTH(Artiest) >= '10'
GROUP BY Artiest
HAVING sum(aantalbeluisteringen) >= 100;