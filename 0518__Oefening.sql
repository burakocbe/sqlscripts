use ModernWays;
set SQL_SAFE_UPDATES = 0;
UPDATE Huisdieren set Geluid = 'WAF!' where Soort = 'hond';
UPDATE Huisdieren set Geluid = 'miauwww...' where Soort = 'kat';
set SQL_SAFE_UPDATES = 1;